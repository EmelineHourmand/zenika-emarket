package fr.zenika.domaine;

/**
 * @author Emeline Hourmand
 */
public class Product {

    private final String name;
    private final double price;

    /**
     * Constructeur de l'objet Product.
     * @param name = nom du produit.
     * @param price = prix du produit.
     */
    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    /**
     * Récupère le nom du produit.
     * @return le nom du produit.
     */
    public String getName() {
        return name;
    }

    /**
     * récupère le prix du produit.
     * @return le prix du produit.
     */
    public double getPrice() {
        return price;
    }

}
