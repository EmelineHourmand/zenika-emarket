package fr.zenika.domaine;

/**
 * Singleton qui permet de récupérer l'instance du client utilisant l'application.
 * @author Emeline Hourmand
 */
public class CustomerManager {

    private static final CustomerManager INSTANCE = new CustomerManager();
    private final Order order = new Order();

    private CustomerManager() {
    }

    /**
     * Récuèpere l'instance du client.
     * @return l'instance du client.
     */
    public static CustomerManager getInstance() {
        return INSTANCE;
    }

    /**
     * Permet de récupérer la commande du client instancié.
     * @return le commande du client.
     */
    public Order getOrder() {
        return order;
    }


}
