package fr.zenika.domaine;

public class ProductBasket {

   private Product product;
   private int quantity;

   public double getTotalPrice() {
       return product.getPrice() * quantity;
   }

    public ProductBasket(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
