package fr.zenika.domaine;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class Order {

    private List<ProductBasket> products = new ArrayList<>();
    private LocalDate orderDate = null;
    private double orderPrice = 0;

    /**
     * Permet d'ajouter un produit dans la commande.
     * @param product = le produit à ajouter
     */
    public void addProduct(ProductBasket product) {
        products.add(product);
    }

    /**
     * Permet de supprimer un produit dans la commande.
     * @param product = le produit à supprimer.
     */
    public void removeProduct(ProductBasket product) {
        products.remove(product);
    }

    /**
     * Permet de récupérer le nombre de produits dans la commande.
     * @return le nombre de produit dans la commande.
     */
    public int getArticlesCount() {
        return products.size();
    }

    /**
     * Permet de récupérer la liste des produits de la commande.
     * @return la liste des produits de la commande.
     */
    public List<ProductBasket> getProducts() {
        return products;
    }

    /**
     * Permet de définir la date de la commande.
     * @param orderDate date de la commande.
     */
    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * Permet de récupérer la date de la commande.
     * @return la date de la commande.
     */
    public LocalDate getOrderDate() {
        return orderDate;
    }

    public double getOrderPrice() {
        for (ProductBasket productBasket : products) {
            this.orderPrice += productBasket.getTotalPrice();
        }
        return orderPrice;
    }

}
