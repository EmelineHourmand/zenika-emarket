package fr.zenika.cli;

import java.util.Scanner;

/**
 * @author Emeline Hourmand
 * Singleton de Scanner pour éviter les duplications de Scanner qui rend instable l'application.
 */
public class ScannerManager {

    private static final Scanner scanner = new Scanner(System.in);
    private static final ScannerManager INSTANCE = new ScannerManager();

    private ScannerManager() {
    }

    /**
     * @return INSTANCE = instance du Scanner de l'application
     */
    public static ScannerManager getInstance() {
        return INSTANCE;
    }

    /**
     * Méthode que permet de récupérer le choix de l'utilisateur.
     * @return int Choice = Le choix de l'utilisateur.
     */
    public int getCustomerChoice() {
        Integer choice = null;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
            } catch (Exception e) {
                System.out.println("Veuillez saisir des chiffres.");
            }
        } while (choice == null);
        return choice;
    }
}
