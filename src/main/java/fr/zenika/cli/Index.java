package fr.zenika.cli;

import fr.zenika.cli.screen.HomeScreen;
import fr.zenika.cli.screen.Screen;

/**
 * @author Emeline Hourmand
 */
public class Index {

    /**
     * Entré de l'application, on redirige vers le premier écran de l'application.
     * @param args argument passé en paramètre de l'application, ici : NULL.
     */
    public static void main(String[] args) {
        Screen.displayFirstScreen(new HomeScreen());
    }
}
