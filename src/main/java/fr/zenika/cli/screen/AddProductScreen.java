package fr.zenika.cli.screen;

import fr.zenika.domaine.CustomerManager;
import fr.zenika.cli.ScannerManager;
import fr.zenika.domaine.Order;
import fr.zenika.domaine.Product;
import fr.zenika.domaine.ProductBasket;

import java.util.List;

/**
 * @author Emeline Hourmand
 */
public class AddProductScreen implements Screen {

    /**
     * Initialisation des produits pour l'application.
     */
    private static final List<Product> vegetablesProducts = List.of(
            new Product("Pomme", 0.50),
            new Product("Poire", 0.45),
            new Product("Banane", 0.33)
    );

    /**
     * Affiche le menu pour ajouter un produit dans le panier du client.
     * @return Le menu principal.
     */
    @Override
    public Screen render() {
        Order order = CustomerManager.getInstance().getOrder();

        System.out.println("****** | Ajout d'un produit | ******");

        for (int i = 0; i < vegetablesProducts.size(); i++) {
            Product product = vegetablesProducts.get(i);
            int availableChoice = i + 1;
            System.out.println(availableChoice + ". "
                    + product.getName() + " Prix : " + product.getPrice() + "€.");
        }

        System.out.println("- Quel article voulez-vous ajouter à votre panier ?");
        int choice = ScannerManager.getInstance().getCustomerChoice();

        if (choice > 0 && choice <= vegetablesProducts.size()) {
            if (order.getProducts().size() > 0) {
                for (int i = 0; i < order.getProducts().size(); i++) {
                    if (order.getProducts().get(i).getProduct().equals(vegetablesProducts.get(choice -1))) {
                        int quantity = order.getProducts().get(i).getQuantity() + 1;
                        order.getProducts().get(i).setQuantity(quantity);
                        return new MainMenuScreen();
                    }
                }
            }

            ProductBasket newProductInBasket = new ProductBasket(vegetablesProducts.get(choice - 1), 1);
            order.addProduct(newProductInBasket);

            return new MainMenuScreen();
        } else {
            System.out.println("Choix incorrect.");
            return this;
        }
    }
}
