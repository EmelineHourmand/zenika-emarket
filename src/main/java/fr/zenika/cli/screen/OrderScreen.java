package fr.zenika.cli.screen;

import fr.zenika.domaine.CustomerManager;
import fr.zenika.domaine.Order;
import fr.zenika.domaine.ProductBasket;

/**
 * @author Emeline Hourmand
 */
public class OrderScreen implements Screen {

    /**
     * Permet d'afficher le panier du client.
     * @return le menu principal.
     */
    @Override
    public Screen render() {
        Order order = CustomerManager.getInstance().getOrder();

        System.out.println("****** | Votre panier | ******");
        double totalOrderPrice = 0;
        for (int i = 0; i < order.getProducts().size(); i++) {
            ProductBasket product = order.getProducts().get(i);
            int availableChoice = i + 1;

            System.out.println(availableChoice + ". " + product.getProduct().getName()
                    + " | Prix unitaire: " + product.getProduct().getPrice() + "€."
                    + " | Quantité : " + product.getQuantity() + "."
                    + " | Prix total : " + product.getTotalPrice() + "€.");

            totalOrderPrice += product.getTotalPrice();
        }

        System.out.println("- Total de votre panier : " + totalOrderPrice + "€.");

        return new MainMenuScreen();
    }
}
