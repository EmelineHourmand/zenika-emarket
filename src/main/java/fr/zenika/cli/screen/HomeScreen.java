package fr.zenika.cli.screen;

/**
 * @author Emeline Hourmand
 */
public class HomeScreen implements Screen {

    /**
     * Afficher le message d'entré de l'application.
     * @return au menu principal.
     */
    @Override
    public Screen render() {
        System.out.println("""
                ****** | BIENVENU SUR ZENIKA E-MARKET ! | ******
                """);
        return new MainMenuScreen();
    }
}