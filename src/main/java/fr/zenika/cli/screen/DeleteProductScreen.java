package fr.zenika.cli.screen;

import fr.zenika.domaine.CustomerManager;
import fr.zenika.cli.ScannerManager;
import fr.zenika.domaine.Order;
import fr.zenika.domaine.Product;

/**
 * @author Emeline Hourmand
 */
public class DeleteProductScreen implements Screen {

    /**
     * Permet de supprimer un produit du panier du client.
     * @return au menu principal.
     */
    @Override
    public Screen render() {
        Order order = CustomerManager.getInstance().getOrder();

        System.out.println("****** | Retrait d'un produit | ******");
        for (int i = 0; i < order.getProducts().size(); i++) {
            Product product = order.getProducts().get(i).getProduct();
            int availableChoice = i + 1;
            System.out.println(availableChoice + ". " + product.getName() + " Prix : " + product.getPrice() + "€.");
        }

        System.out.println("- Quel produit voulez-vous retirer de votre panier ?");

        int choice = ScannerManager.getInstance().getCustomerChoice();

        if (choice > 0 && choice <= order.getProducts().size()) {
            order.removeProduct(order.getProducts().get(choice - 1));
            return new MainMenuScreen();
        } else {
            System.out.println("Choix incorrect.");
            return this;
        }
    }
}
