package fr.zenika.cli.screen;

import fr.zenika.domaine.CustomerManager;
import fr.zenika.domaine.Order;

import java.time.LocalDate;

/**
 * @author Emeline Hourmand
 */
public class FinishScreen  implements Screen {

    /**
     * Afficher l'écran de fin de l'application.
     * @return null pour quitter l'application.
     */
    @Override
    public Screen render() {
        Order order = CustomerManager.getInstance().getOrder();
        if (order.getProducts().size() > 0) {
            order.setOrderDate(LocalDate.now());

            System.out.println("Votre commande du " + order.getOrderDate() +
                    ",\npour un total de " + order.getOrderPrice() + "€ a bien été validé.");
        }
        System.out.println("""
                
                ****** | MERCI DE VOTRE VISITE | ******
                             A bientôt !
                """);
        return null;
    }
}
