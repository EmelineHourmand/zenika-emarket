package fr.zenika.cli.screen;

import fr.zenika.cli.ScannerManager;

/**
 * @author Emeline Hourmand
 */
public class MainMenuScreen implements Screen {

    /**
     * Permet d'afficher le menu principal et de rediriger le client en fonction de son choix.
     * @return un écran en fonction du choix du client.
     */
    @Override
    public Screen render() {

        System.out.println("""
        ****** | MENU PRINCIPAL | ******
        1) Ajouter un produit au panier.
        2) Retirer un produit du panier.
        3) Visualiser le panier.
        4) Quitter l'application.
        """);

        System.out.println("- Que souhaitez-vous faire ?");

        int choice = ScannerManager.getInstance().getCustomerChoice();

        return switch (choice) {
            case 1 -> new AddProductScreen();
            case 2 -> new DeleteProductScreen();
            case 3 -> new OrderScreen();
            case 4 -> new FinishScreen();
            default -> this;
        };
    }
}
