package fr.zenika.cli.screen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScreenTest {

    class FakeScreen implements Screen {
        private boolean isRendered = false;
        private Screen nextScreen;

        FakeScreen(Screen nextScreen) {
            this.nextScreen = nextScreen;
        }

        @Override
        public Screen render() {
            isRendered = true;
            return nextScreen;
        }

        public boolean isRendered() {
            return isRendered;
        }
    }

    @Test
    void shouldChainScreens() {
        FakeScreen screen3 = new FakeScreen(null);
        FakeScreen screen2 = new FakeScreen(null);
        FakeScreen screen1 = new FakeScreen(screen2);

        Screen.displayFirstScreen(screen1);

        Assertions.assertTrue(screen1.isRendered());
        Assertions.assertTrue(screen2.isRendered());
        Assertions.assertFalse(screen3.isRendered());
    }
}