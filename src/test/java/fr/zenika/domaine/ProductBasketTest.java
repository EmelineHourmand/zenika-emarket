package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductBasketTest {

    Product product1 = new Product("Fraise", 1.5);
    ProductBasket productBasket1 = new ProductBasket(product1, 1);

    @Test
    void get_total_price() {
        assertEquals(1.5, productBasket1.getTotalPrice());
    }

    @Test
    void get_product() {
        assertEquals(product1, productBasket1.getProduct());
    }

    @Test
    void get_quantity() {
        assertEquals(1, productBasket1.getQuantity());
    }

    @Test
    void set_quantity() {
        productBasket1.setQuantity(2);
        assertEquals(2, productBasket1.getQuantity());
    }
}