package fr.zenika.domaine;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    Product product1 = new Product("Fraise", 5);

    @Test
    void getName() {
        assertEquals("Fraise", product1.getName());
    }

    @Test
    void getPrice() {
        assertEquals(5, product1.getPrice());
    }

}