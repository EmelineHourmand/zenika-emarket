package fr.zenika.domaine;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    ProductBasket product1 = new ProductBasket(new Product("Fraise", 5), 2);
    ProductBasket product2 = new ProductBasket(new Product("Cerise", 6), 1);

    Order order = new Order();

    @Test
    void add_one_product() {
        order.addProduct(product1);
        assertEquals(1, order.getArticlesCount());
    }

    @Test
    void add_multiple_products() {
        order.addProduct(product1);
        order.addProduct(product2);
        assertEquals(2, order.getArticlesCount());
    }

    @Test
    void remove_product() {
        order.addProduct(product1);
        order.addProduct(product2);
        order.removeProduct(product1);
        assertEquals(1, order.getArticlesCount());
    }

    @Test
    void get_articles_count() {
        order.addProduct(product1);
        order.addProduct(product2);
        assertEquals(2, order.getArticlesCount());
    }

    @Test
    void get_product() {
        order.addProduct(product1);
        order.addProduct(product2);
        assertEquals(product1, order.getProducts().get(0));
    }

    @Test
    void get_order_date() {
        order.setOrderDate(LocalDate.now());
        assertEquals(LocalDate.now(), order.getOrderDate());

    }

    @Test
    void get_total_order_price() {
        order.addProduct(product1);
        order.addProduct(product2);
        assertEquals(16, order.getOrderPrice());
    }
}